var express      = require('express');
var path         = require('path');
var favicon      = require('static-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var dbConfig     = require('./db');
var mongoose     = require('mongoose');
var moment       = require('moment');
var multer       = require('multer');


// Connect to DB
mongoose.connect(dbConfig.url);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Configuring Passport
var passport = require('passport');
// TODO - Why Do we need this key ?
app.use(session({
	secret: '306cd528a01d56b7417badf1921188a9',
    proxy: true,
    resave: true,
    saveUninitialized: true,
	cookie: {
		maxAge: 600000
	}}));
app.use(passport.initialize());
app.use(passport.session());
var upload = multer({ dest: './public/uploads' })
 // Using the flash middleware provided by connect-flash to store messages in session
 // and displaying in templates
var flash = require('connect-flash');
app.use(flash());

// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);

var routes = require('./routes/index')(passport);
app.use('/', routes);
app.use('/api', require('./routes/api'));
app.use('/logbook', require('./routes/logbook'));
app.use('/search', require('./routes/search'));
app.use('/user', require('./routes/user'));
app.use('/ajax', require('./routes/ajax'));
app.use('/user', require('./routes/user'));
app.use('/callsign', require('./routes/callsign'));
app.use('/rest', require('./routes/rest'));



app.use(function (req, res, next) {
  res.locals.login = req.isAuthenticated();
  next();
});
/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

var allowCrossDomain = function(req,res,next){
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers','Content-Type, Authorization, Content-Length,X-Requested-With');
    next();
}
app.use(allowCrossDomain);
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
app.set('port', 3002);
app.listen(app.get('port'));
module.exports = app;
