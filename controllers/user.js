// Load required packages
var User = require('../models/user');

// End-point /api/users for POST
exports.postUsers = function(req, res) {

  var user = new User();

  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.callsign = req.body.callsign;
  user.email = req.body.email;
  user.password = createHash(req.body.password);
  user.admin = req.body.admin;

  user.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'User added to the system!', data: user });
  });
};

// Create endpoint /api/users for GET
exports.getUsers = function(req, res) {

  User.find(function(err, users) {
    if (err)
      res.send(err);

    res.json(users);
  });
};

// Create endpoint /api/beers/:uid for GET
exports.getUser = function(req, res) {

  User.findById(req.params.uid, function(err, user) {
    if (err)
      res.send(err);

    res.json(user);
  });
};

// Create endpoint /api/users/:uid for PUT
exports.putUser = function(req, res) {
  
  User.findById(req.params.uid, function(err, user) {
    if (err)
      res.send(err);

    user.email = req.body.email;

    user.save(function(err) {
      if (err)
        res.send(err);

      res.json(user);
    });
  });
};

// Create endpoint /api/users/:uid for DELETE
exports.deleteUser = function(req, res) {

  User.findByIdAndRemove(req.params.uid, function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'User removed from the system!' });
  });
};

// Generates hash using bCrypt
var createHash = function(password){
	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}