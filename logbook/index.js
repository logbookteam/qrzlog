Logbook = require("../models/logbook");

module.exports = function(user_id,callback){
        var today = new Date();
        var todayMinus1Week = new Date();
        todayMinus1Week = todayMinus1Week.setDate(todayMinus1Week.getDate() - 7);
        console.log(today + " | " + todayMinus1Week);
        Logbook.aggregate(
            [
                { "$match": {
                    "time": { 
                        "$gte": new Date(todayMinus1Week),"$lte": new Date(today)
                    },
                    "uid": user_id
                }},
                { "$project": {
                    "yearMonthDay": { "$dateToString": { "format": "%Y-%m-%d", "date": "$time" } },
                    "time": { "$dateToString": { "format": "%H:%M:%S:%L", "date": "$timee" } }
                }},
                { "$group": {
                    "_id": { 
                        "day":   { "$dayOfMonth": "$time" },
                        "month": { "$month": "$time"},
                        "year":  { "$year": "$time"},
                    },
                    "count": { "$sum": 1 }
                }},
                { "$sort": {
                    time: -1
                }}
            ],
            function(err,result) {
                if (err) {
                  console.log("Error:", err);
                  return callback(err);
                } else {
                  console.log("Result ["+user_id+"]: %j", result);
                  return callback(null, result);
                }
              }
        );

};