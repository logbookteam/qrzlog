var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var session = require('express-session');

var sess;

var logbookSchema = new Schema({
	id: String,
	uid: String,
	lid: { type: String, unique: true},
	time : { type : Date, default: Date.now },
	callsign: String,
	contact: String,
	start: String,
	end: String,
	frequency: String,
	band: String,
	mode: String,
	RSTsent: String,
	RSTrec: String,
	notes: String
})

var Logbook = mongoose.model('Logbook', logbookSchema);

// make this available to our users in our Node applications
module.exports = Logbook;
