var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
	id: String,
	uid: String,
	secret: String,
	password: { type: String, required: true},
	admin: { type: Boolean, required: true},
	callsign: { type: String, required: true, unique: true},
	email: { type: String, required: true},
	firstName: { type: String, required: true},
	lastName: { type: String, required: true},
	lat: { type: String},
	lng: { type: String},
	dxcc: { type: String},
	dxccid: { type: String},
	profilePublic: Boolean,
	logPublic: Boolean,
	marketing: Boolean,
	profileImg: { type: String },
	profileImgThumb: { type: String },
	profileImgMed: { type: String },
	locationTown: { type: String },
	locationCountry: { type: String },
	profileImg: { data: Buffer, contentType: String },
	created_at: { type : Date },
	updated_at: { type : Date, default: Date.now }
})

  
userSchema.methods.verifyPassword = function(password, cb) {
  bcrypt.compareSync(password, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

var User = mongoose.model('User', userSchema);

// Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

// make this available to our users in our Node applications
module.exports = User;