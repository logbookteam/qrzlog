var LocalStrategy   = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');



module.exports = function(passport){

	passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, callsign, password, done) { 
            // check in mongo if a user with callsign exists or not
			callsign = callsign.toUpperCase();
            User.findOne({ 'callsign' :  callsign }, 
                function(err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // callsign does not exist, log the error and redirect back
                    if (!user){
                        console.warn('User Not Found with callsign '+callsign);
                        return done(null, false, req.flash('message', 'User Not found.'));                 
                    }
                    // User exists but wrong password, log the error 
                    if (!isValidPassword(user, password)){
                        console.warn('Invalid Password');
                        return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                    }
                    
                    // User and password both match, return user from done method
                    // which will be treated like success
					var sess = req.session;
					sess.uid = user.uid;
					sess.firstName = user.firstName;
					sess.email = user.email;
					sess.admin = user.admin;
					req.session.save(function(err) {
					return done(null, user, sess);
					});
                    
                }
            );

        })
    );


    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }
    
}