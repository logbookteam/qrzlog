var LocalStrategy   = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var md5 = require('MD5');
var mailer = require('../models/mailer');
var crypto = require('crypto');
function randomValueHex (len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
}
module.exports = function(passport){

	passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, callsign, password, done) {

            findOrCreateUser = function(){
				callsign = callsign.toUpperCase();
                // find a user in Mongo with provided callsign
                User.findOne({ 'callsign' :  callsign }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err){
                        console.warn('Error in SignUp: '+err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.warn('User already exists with callsign: '+callsign);
                        return done(null, false, req.flash('message','callsign already exists at qrzlog.com'));
                    } else {
                        // if there is no user with that email
                        // create the user
                        var newUser = new User();
						
                        // set the user's local credentials
						date = new Date();
						newUser.uid = createHash(date+callsign);
						newUser.secret = randomValueHex(32);
                        newUser.callsign = callsign;
                        newUser.password = createHash(password);
                        newUser.email = req.param('email');
                        newUser.firstName = req.param('firstName');
                        newUser.lastName = req.param('lastName');
						newUser.admin = false;
						newUser.created_at = date;
						newUser.updated_at = date;

                        var objs = {
                                email: req.param('email'),
                                subject: 'Welcome to QRZLog.com',
                                name: req.param('firstName')
                            };
                            console.log(objs);
                            mailer.sendOne('welcome',objs,function(err,response){
                                if(err){console.log(err);}
                                    console.log('Welcome email sent'+response);
                        });

                        // save the user
                        newUser.save(function(err) {
                            if (err){
                                console.warn('Error in Saving user: '+err);  
                                throw err;  
                            }
                            console.warn('User Registration succesful');    
                            return done(null, newUser);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );

    // Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}