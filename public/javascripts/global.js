$(document).ready(function(){
	$("#searchCallsign").submit(function(e){
		e.preventDefault();
		var call = $('[name="searchCall"]').val();
		window.location = '/search/' + call;
	});
  $("#mobileSearchCallsign").submit(function(e){
    e.preventDefault();
    var call = $('#mobileSearchCallsign [name="searchCall"]').val();
    window.location = '/search/' + call;
  });
	$('#fileUpload').submit(function() {
		$("#response").html("<p>We're just making your image pretty...</p>");
        $(this).ajaxSubmit({
            error: function(xhr) {
                status('Error: ' + xhr.status);
            },
            success: function(data) {
                if(data.status=="success"){
					$("#response").html('<p class="success">'+data.message+'</p>');
					$(".currentImg").attr('src',data.url);
				}else{
					$("#response").html('<p class="fail">'+data.message+'</p>');
				}
            }
    });
    return false;
    });  

$(function () {

    $("#search").autocomplete({

        source: function( request, response ) {
          $.ajax({
              url: "/ajax/dBsearch",
              dataType: "jsonp",
              data: {
                 featureClass: "P",
                 style: "full",
                 maxRows: 12,
                 callsign: request.term
              },
              success: function( data ) {
                  response( $.map( data.results, function( item ) {
                       return {
                          label: item,
                          value: item
                       }
                  }));
             }
          });
       }

    });

  });	
  //$(".mobileSearch").on("click",function(){
  //  $("#mobileSearch").slideToggle('fast');
  //});

});