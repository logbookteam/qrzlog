var express = require('express');
var router = express.Router();
var session = require('express-session');
var util = require("util"); 
var User = require('../models/user');
var Logbook = require('../models/logbook');
var passport = require('passport');
var fs = require('fs');
var easyimg = require('easyimage');


router.post('/imgUpload', function(req,res){
	if (req.files) { 
		console.log(util.inspect(req.files));
		if (req.files.myFile.size === 0) {
		            return next(new Error("Hey, first would you select a file?"));
		}
		fs.exists(req.files.myFile.path, function(exists) { 
			if(exists) { 
				User.findOne( {"uid":req.session.uid}, function ( err, user ){
					console.log(req.session.uid);
					console.log(req.files.myFile.name);

					user.profileImg  = '/uploads/'+req.files.myFile.name;
					user.profileImgThumb = '/uploads/thumb_'+req.files.myFile.name;
					user.profileImgMed = '/uploads/med_'+req.files.myFile.name;
					user.updated_at = Date.now();
					user.save( function ( err, user, count ){
					  if( err ) return next( err );
						easyimg.rescrop({
							 src: '/home/qrzlog/www/public/uploads/'+req.files.myFile.name, dst:'/home/qrzlog/www/public/uploads/thumb_'+req.files.myFile.name,
							 width:320, height:320,
							 cropwidth:256, cropheight:256,
							 x:0, y:0
						  }).then(
						  function(image) {
							 console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
							 easyimg.rescrop({
							 src: '/home/qrzlog/www/public/uploads/'+req.files.myFile.name, dst:'/home/qrzlog/www/public/uploads/med_'+req.files.myFile.name,
							 width:500, height:500,
							 cropwidth:400, cropheight:400,
							 x:0, y:0
							  }).then(
							  function(image) {
								 console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
								 
								 res.json({status:'success',message:'Uploaded image',url:'/uploads/thumb_'+req.files.myFile.name});
							  },
							  function (err) {
								console.log(err);
							  }
							);
							
						  },
						  function (err) {
							console.log(err);
						  }
						);

					});
				  });
			} else { 
				res.json({ status: 'error', message: 'Well, there is no magic for those who don’t believe in it!'});
			} 
		}); 
	} 
});

router.get('/dbSearch', function(req,res){
	
	User.find({'callsign' : new RegExp(req.param.callsign, "i") }, function (err, users){

    if (err) throw err;

    var names = [];

    for (var nam in users) {

          names.push(users[nam].user);

    }

    var result = { results: names }

    res.json(result);

    });
});

module.exports = router;
