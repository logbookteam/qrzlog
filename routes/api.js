var express = require('express');
var router = express.Router();
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var bCrypt = require('bcrypt-nodejs');

router.post('/authenticate', function(req, res) {

	// find the user
	User.findOne({
		callsign: req.body.callsign
	}, function(err, user) {

		if (err) throw err;

		if (!user) {
			res.json({ success: false, message: 'Authentication failed. User not found.' });
		} else if (user) {

			// check if password matches
				if (!isValidPassword(user, req.body.password)){
                        console.warn('Invalid Password');
                        res.json({success: false, message: 'Incorrect password.'});
                }else {

				// if user is found and password is right
				// create a token
				//var token = jwt.sign(user, 'OurSuperSecret2015', {
				//	expiresInMinutes: 1440 // expires in 24 hours
				//});
				var token = user.secret;
				res.json({
					success: true,
					token: token,
					nicename: user.firstName
				});
				console.log("Token: " + token);
			}		

		}

	});
});

// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
router.use(function(req, res, next) {

	// check header or url parameters or post parameters for token
	var token = req.body.token || req.param('token') || req.headers['x-access-token'];

	// decode token
	if (token) {
		User.findOne({
		secret: token
		}, function(err, user) {
		// verifies secret and checks exp
		//jwt.verify(token, 'OurSuperSecret2015', function(err, decoded) {			
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });		
			}else if(!user){
				return res.json({success: false, message: 'Invalid access token'});
			} else {
				// if everything is good, save to request for use in other routes
				console.log(user);
				req.user = user;	
				next();
			}
		});

	} else {

		// if there is no token
		// return an error
		return res.status(403).send({ 
			success: false, 
			message: 'No token provided.'
		});
		
	}
	
});

// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------
router.get('/', function(req, res) {
	res.json({ message: 'Welcome to the coolest API on earth!' });
});

router.get('/users', function(req, res) {
	if(req.user.admin == true){
		User.find({}, function(err, users) {
			res.json(users);
		});
	}else{
		res.json({
			message: 'You are not authorized to view this'
		});
	}
	
});

router.get('/check', function(req, res) {
	res.json(req.user);
});

module.exports = router;
var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }