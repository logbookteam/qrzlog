var express = require('express');
var router = express.Router();
var User = require('../models/user');


/* GET users listing. */
router.get('/:callsign', function(req, res, next) {
	User.findOne({callsign: req.param("callsign")}, function(err,call){
		if(err){
			console.log(err);
			req.flash('message','Sorry, something went wrong. Try again.');
			res.render('404'),{
				message: req.flash('message'), 
				title: 'Sorry, something went wrong!'
			}
		}
		if(call.length > 0){
			/*console.log("Query: " + call);*/
			res.render('userProfile',{
				prof: call,
				title: req.param("callsign") + '\'s Profile'
			});
		}else{
			res.render('404',{
				message: req.flash('We can\'t find that user, sorry!')
			});
		}
	});
});

module.exports = router;
