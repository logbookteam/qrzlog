var express = require('express');
var router = express.Router();
var session = require('express-session');
var Logbook = require('../models/logbook');
var LogbookData = require('../logbook/index.js');
var User = require('../models/user');
var moment = require('moment');
var sess;

sess = session;

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

module.exports = function(passport){

	/* GET login page. */
	router.get('/',function(req, res, next) {
			Logbook.find({},{},{ limit: 10, sort: {time: -1}}, function(err,logbook){
				if (err) { console.log(err); return next();}
				// Display the Login page with any flash message, if any
				res.render('index', { 
					message: req.flash('message'), 
					title: 'The portable logbook | QRZLog',
					recentLogs: logbook,
					moment: moment,
					user: req.user,
					page: 'home'
				});
			});
	});

	router.get('/about-qrzlog',function(req,res){
		res.render('about-qrzlog',{
			title: 'About QRZlog.com | The online portable logbook for radio amateurs'
		});
	});

	/* Handle Login POST */
	router.post('/login', passport.authenticate('login', {
		successRedirect: '/dashboard',
		failureRedirect: '/login',
		failureFlash : true  
	}));
	
	/* Handle Login Page */
	router.get('/login', function(req,res){
		res.render('login',{
			message: req.flash('message'),
			title: 'Log in to your QRZLog.com account'
		});
	});

	/* GET Registration Page */
	router.get('/signup', function(req, res){
		res.render('register',{
			message: req.flash('message'),
			title: 'Register an account on QRZLog.com'
		});
	});

	/* Handle Registration POST */
	router.post('/signup', passport.authenticate('signup', {
		successRedirect: '/dashboard',
		failureRedirect: '/signup?error',
		failureFlash : true  
	}));
	
	router.get('/dashboard', isAuthenticated, function(req, res, next) {
		Logbook.find({ 'uid' :  req.user.uid }, {}, { limit: 10}, function(err, logbook){
			if (err) { console.log(err); return next(err); }
			
			LogbookData(req.user.uid, function(err, entries) {
		      if (err) { console.log(err); return next(err); }
		      res.render('dashboard', { 
		        user: req.user,
		        logbook: logbook,
		        locals: {
		          firstName: req.session.firstName,
		          email: req.session.email,
		          admin: req.session.admin
		        },
		        title: 'QRZLog Dashboard v1.0',
		        recentEntries: entries
		      });
		      console.log(entries);
		    });
		});
	});
	router.get('/myaccount', isAuthenticated, function(req,res,next){
		User.findOne({ 'uid' : req.user.uid}, {}, function(err, user){
			res.render('myaccount', {
				user: req.user,
				title: 'My account at QRZLog.com'
			});
		});
	});
	router.get('/users', isAuthenticated, function(req, res, next) {
		User.find({ }, function(err, user){
			if(err){
				console.log(err);
				return next();
			}
			res.render('userlist', {
				locals: {
					users: user,
					admin: req.session.admin,
					firstName: req.session.firstName
				},
				title: 'User list'
			});

		});
	});

	/* Handle Logout */
	router.get('/signout', function(req, res) {
		req.logout();
		req.session.destroy(function(err){
		if(err){
		console.log(err);
		}
		else
		{
		res.redirect('/');
		}
		});
	});
	
	return router;
}
