var express = require('express');
var router = express.Router();
var session = require('express-session');
var Logbook = require('../models/logbook');
var bCrypt = require('bcrypt-nodejs');

var sess;
function loggedIn(req, res, next) {
    if (req.user) {
        next();
    } else {
        res.redirect('/');
    }
}
function getUserEntries(req, res, next){
	Logbook.find({ 'uid' :  req.user.uid }, function(err, logbook) {
		res.render('logbookView',{
			log: logbook,
			user: req.user,
			title: 'Your logbook'
		})
	});
}

function getSingleLog(req,res){
	Logbook.findOne({'_id' : req.params.id, 'uid' : req.user.uid}, function(err, entry){
		if(err)
		{
			console.log('No entries found'+err);
		}
		if(entry){
			console.log(entry);
			res.render('logbookEntry',{
				log: entry,
				user: req.user,
				title: 'Single logbook entry:'+req.params.id
			})
		} else {
			console.log('No entries found');
		}
	});
}

router.get('/',loggedIn, function(req,res){
	getUserEntries(req,res);
});
router.get('/edit/:id',loggedIn,function(req,res){
	getSingleLog(req,res);	
})
router.get('/add',loggedIn,function(req,res){
	res.render('logbookEntryAdd',{
		user: req.user,
		title: 'Add a logbook entry'
	});
});
router.post('/add',loggedIn,function(req,res){
	var newLogbook = new Logbook();
	
	newLogbook.uid = req.user.uid;
	newLogbook.lid = createHash(req.user.callsign + req.body.contact);
	newLogbook.callsign = req.user.callsign;
	newLogbook.contact = req.body.contact;
	newLogbook.start = req.body.start;
	newLogbook.end = req.body.end;
	newLogbook.frequency = req.body.frequency;
	newLogbook.band = req.body.band;
	newLogbook.mode = req.body.mode;
	newLogbook.RSTsent = req.body.RSTsent;
	newLogbook.RSTrec = req.body.RSTrec;
	newLogbook.notes = req.body.notes;
	
	 newLogbook.save(function(err) {
		if (err){
			console.warn('Error in inserting logbook entry: '+err);  
			throw err;  
		}
		console.warn('Logbook entry successful');    
		res.redirect("/logbook");
	});
})

var createHash = function(string){
        return bCrypt.hashSync(string, bCrypt.genSaltSync(5), null);
    }

module.exports = router;




