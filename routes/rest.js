var express = require('express');
var http = require('http');
var parseString = require('xml2js').parseString
var et = require('elementtree');

var XML = et.XML;
var ElementTree = et.ElementTree;
var element = et.Element;
var subElement = et.SubElement;

var etree,key;

var router = express.Router();

function fetchSessionKey(username, password, callback) {
  http.get({
  	host: 'xmldata.qrz.com',
	path: '/xml/current/?username=2e1ick&password=amateur1',
	method: 'GET'
  }, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      var key = et.parse(chunk).findtext('./Session/Key');
      return callback(null, key);
    });
  }).end();
}

function fetchCallsignInformation(callsign,sessionKey, callback) {
  // Do your request for callsign information and return the callback with data
  http.get({
  	host: 'xmldata.qrz.com',
	path: '/xml/current/?s='+sessionKey+'&callsign='+callsign,
	method: 'GET'
  }, function (response) {
    response.setEncoding('utf8');
    response.on('data', function (chunk) {
      var fname = et.parse(chunk).findtext('./Callsign/fname');
	  var name = et.parse(chunk).findtext('./Callsign/name');
	  var country = et.parse(chunk).findtext('./Callsign/country');
	  
	  return callback(null, {
		fname: fname,
		name: name,
		country: country
		// ...
	  });
    });
  }).end();
  
}


router.get('/:lookup',function(req,res){
	fetchSessionKey("2e1ick", "amateur1", function (err, sessionKey) {
		fetchCallsignInformation(req.param("lookup"),sessionKey, function (err, callsignInformation) {
		  if(!err){
			  res.render('callsignView',{
				  call: callsignInformation
			  });
			  console.log(callsignInformation.fname);
			  console.log(callsignInformation.name);
			  console.log(callsignInformation.country);
		  }
		});
	  });
});
 
module.exports = router;