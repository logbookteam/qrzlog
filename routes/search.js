var express = require('express');
var router = express.Router();
var User = require('../models/user');

var escape = RegExp.escape = function(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

router.get('/:callsign', function(req,res){
	var search = req.params.callsign;
	var term = escape(search);
	term = term.toUpperCase();
	if(search=="*" || search==""){
		res.redirect("/");
	}
	User.find({'callsign' : new RegExp(term, "i") }, function(err, callsign){
		if(err)
		{
			console.log('No user found'+err);
			req.flash('message','Sorry, something went wrong. Try again.');
			res.render('callSearchResults'),{
				message: req.flash('message'), 
				title: 'Sorry, no results'
			}
		}
		if(callsign.length > 0){
			console.log('Callsign:'+callsign+term);
			res.render('callSearchResults',{
				call: callsign,
				title: 'You searched for '+search,
				query: term
			});
		}else{
			console.log('No entries found'+search);
		}
	});
});

module.exports = router;




