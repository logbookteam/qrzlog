var express = require('express');
var router = express.Router();
var User = require('../models/user');


/* GET users listing. */
router.get('/:callsign', function(req, res, next) {
	User.findOne({callsign: req.params.callsign}, function(err,call){
		if(err){
			console.log(err);
			req.flash('message','Sorry, something went wrong. Try again.');
			res.render('404'),{
			message: req.flash('message'), 
				title: 'Sorry, something went wrong!'
			}
		}
		if(call){
			res.render('userProfile'),{
				call: call,
				title: call + '\'s Profile'
			}
		}
	});
});

module.exports = router;
